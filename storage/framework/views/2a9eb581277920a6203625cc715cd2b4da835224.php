<?php $__env->startSection('content'); ?>
<div class="row" id="home">
	<div class="span12">
		<div class="widget widget-nopad">
			<div class="widget-header"> <i class="icon-list-alt"></i>
				<h3> Today's Status</h3>
			</div>
			<!-- /widget-header -->
			<div class="widget-content">
				<div class="widget big-stats-container">
					<div class="widget-content">
						<!-- <h6 class="bigstats">A fully responsive premium quality admin template built on Twitter Bootstrap by <a href="http://www.egrappler.com" target="_blank">EGrappler.com</a>.  These are some dummy lines to fill the area.</h6> -->
						<div id="big_stats" class="cf">
							<div class="stat"> Employees <i class="icon-user"></i> <span class="value"><?php echo e($empcount); ?></span> </div>
							<!-- .stat -->

							<div class="stat"> Partys <i class="icon-group"></i> <span class="value"><?php echo e($partycount); ?></span> </div>
							<!-- .stat -->

							<div class="stat"> Stock <i class="icon-money"></i> <span class="value"><?php echo e($sumStock); ?></span> </div>
							<!-- .stat -->

							<div class="stat">Others <i class="icon-bullhorn"></i> <span class="value">0%</span> </div>
							<!-- .stat --> 
						</div>
					</div>
					<!-- /widget-content --> 

				</div>
			</div>
		</div>
		<!-- /widget -->
		<div class="widget widget-nopad">
			<div class="widget-header"> <i class="icon-list-alt"></i>
				<h3> Recent News</h3>
			</div>
			<!-- /widget-header -->
			<div class="widget-content">
				<div id='calendar'>
				</div>
			</div>
			<!-- /widget-content --> 
		</div>
		<!-- /widget -->

	</div>

	<!-- /widget-content --> 
</div>
<!-- /widget -->
</div>
<!-- /span6 --> 
</div>
<!-- /row --> 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>