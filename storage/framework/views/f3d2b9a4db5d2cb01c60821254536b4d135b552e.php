<?php $__env->startSection('content'); ?>
<div class="row">	
   <div class="span12">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>Add New Party</h3>
         </div>
         <!-- /widget-header -->
         <div class="widget-content">
            <div class="tabbable">              
               <div class="tab-content">
                  <div class="tab-pane active" id="formcontrols">
                     <form id="edit-profile" class="form-horizontal" action="<?php echo e(url('/store')); ?>" method="POST">
                     	<?php echo csrf_field(); ?>
                        <?php if(Session::get('error')): ?>
                           <div class="alert alert-danger">
                              <?php echo e(Session::get('error')); ?>

                           </div>
                           <?php endif; ?>
                           <?php if(session('success')): ?>
                              <div class="alert alert-success">
                                <?php echo e(session('success')); ?>

                              </div>
                           <?php endif; ?>
                           <?php if(session('danger')): ?>
                              <div class="alert alert-danger">
                                <?php echo e(session('danger')); ?>

                              </div>
                           <?php endif; ?>
                        <fieldset>
                           <div class="control-group">
                              <label class="control-label" for="firm_name">Firm name</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="firm_name" placeholder="Enter Your firm_name" name="firm_name" required="true" autofocus="ture">
                              </div>
                              <!-- /controls -->				
                           </div>
                           <div class="control-group">
                              <label class="control-label" for="gst_no">GST NO.</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="gst" placeholder="Enter Your GST Number" name="gst">
                              </div>
                              <!-- /controls -->				
                           </div>
                           <!-- /control-group -->
                           <div class="control-group">
                              <label class="control-label" for="firstname">First Name</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="firstname" name="firstname" placeholder="Enter Your First Name">
                              </div>
                              <!-- /controls -->				
                           </div>
                           <!-- /control-group -->
                           <div class="control-group">
                              <label class="control-label" for="lastname">Last Name</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="lastname" name="lastname" placeholder="Enter Your Last Name">
                              </div>
                              <!-- /controls -->				
                           </div>
                           <!-- /control-group -->
                           <div class="control-group">
                              <label class="control-label" for="email">Email Address</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="email" name="email" placeholder="Enter Valid Email Address">
                              </div>
                              <!-- /controls -->				
                           </div>
                           <!-- /control-group -->
                           <div class="control-group">
                              <label class="control-label" for="phone">Phone</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="phone" name="phone" placeholder="Enter phone Number" required="true">
                              </div>
                              <!-- /controls -->				
                           </div>
                           <!-- /control-group -->
                           <div class="control-group">
                              <label class="control-label" for="address">Address</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="address" name="address" placeholder="Enter Address">
                              </div>
                              <!-- /controls -->				
                           </div>
                           <!-- /control-group -->
                           <div class="form-actions">
                              <button type="submit" class="btn btn-primary">Save</button> 
                              <button class="btn btn-default" type="reset">Reset</button>
                           </div>
                           <!-- /form-actions -->
                        </fieldset>
                     </form>
                  </div>                  
               </div>
            </div>    
            <div class="span11">
               <div class="widget-header">
                  <h3>Show Records</h3>
               </div>
               <!-- /widget-header -->
               <div class="widget-content">
                  <div class="card-body">
               <table class="table table-bordered">
                  <thead>
                     <tr>
                        <th>Sr.</th>                        
                        <th>Firm Name</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone No.</th>
                        <th>Address</th>
                        <th>GST No.</th>
                        <th>Created At</th>
                        <th>Edit</th>
                        <th>Delete</th>
                     </tr>
                  </thead>
                  <?php  $SrNo = 1; ?>
                  <tbody>
                     <?php $__currentLoopData = $partydata; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                   
                     <tr>
                        <td><?php echo e($SrNo++); ?></td>
                        <td><?php echo e($data->firm_name); ?></td>
                        <td><?php echo e($data->firstname); ?>  <?php echo e($data->lastname); ?></td>
                        <td><?php echo e($data->email); ?></td>
                        <td><?php echo e($data->phone); ?></td>
                        <td><?php echo e($data->address); ?></td>
                        <td><?php echo e($data->gst); ?></td>
                        <td><?php echo e($data->created_at->format('d/m/Y H:i:s')); ?></td>                         
                        <td>
                           <a href="<?php echo e(url('edit', $data['id'])); ?>" class="btn btn-warning btn-sm"><i class="icon-pencil"></i></a>
                        <td>
                           <form action="<?php echo e(url('destroy', $data['id'])); ?>" method="post">
                              <?php echo csrf_field(); ?>
                              <input name="_method" type="hidden" value="DELETE">
                              <button class="btn btn-danger btn-sm" type="submit"><i class="icon-remove"></i></button>
                            </form>
                        </td>                         
                     </tr>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
               </table>             
            </div>
               </div>
            </div>
         </div>
         <!-- /widget-content -->
      </div>
      <!-- /widget -->
   </div>
   <!-- /span8 -->
</div>
<!-- /row -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>