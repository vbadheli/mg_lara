<div class="navbar navbar-fixed-top noprint">
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="<?php echo e(url('/home')); ?>">Meragu Garments</a>
      <div class="nav-collapse">
        <ul class="nav pull-right">
          <!-- <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-cog"></i> Account <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="javascript:;">Settings</a></li>
              <li><a href="javascript:;">Help</a></li>
            </ul>
          </li> -->
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-user"></i> <?php echo e(Auth::user()->username); ?> <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="javascript:;">Profile</a></li>
              <li><a href="javascript:;" href="<?php echo e(url('logout')); ?>" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                    </a>
                <form id="logout-form" action="<?php echo e(url('logout')); ?>" method="POST" style="display: none;">
                  <?php echo csrf_field(); ?>
                </form>
              </li>
            </ul>
          </li>
        </ul>
        <!-- <form class="navbar-search pull-right">
          <input type="text" class="search-query" placeholder="Search">
        </form> -->
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
<!-- /navbar -->
<div class="subnavbar noprint">
  <div class="subnavbar-inner">
    <div class="container">
      <ul class="mainnav">
        <li class="active"><a href="<?php echo e(url('/home')); ?>"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>
        <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user"></i><span>Master</span> </a>
            <ul class="dropdown-menu">
              <li><a href="<?php echo e(url('/party')); ?>">Partys</a></li>
              <li><a href="<?php echo e(url('/employee')); ?>">Employees</a></li>             
              <li><a href="<?php echo e(url('/size')); ?>">Size</a></li>             
              <li><a href="<?php echo e(url('/cutting')); ?>">Cutting</a></li>             
            </ul>
        </li>
        <li><a href="<?php echo e(url('/stock')); ?>"><i class="icon-money"></i><span>Stock</span></a></li> 
        <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-list-alt"></i><span>Reports</span> </a>
            <ul class="dropdown-menu">
              <li><a href="<?php echo e(url('/report/party/show')); ?>">Party</a></li>
              <li><a href="<?php echo e(url('/report/employee/show')); ?>">Employee</a></li>
              <li><a href="<?php echo e(url('/report/show')); ?>">Stock</a></li>             
              <li><a href="<?php echo e(url('/report/size/show')); ?>">Size</a></li>             
            </ul>
        </li>              
      </ul>
    </div>
    <!-- /container --> 
  </div>
  <!-- /subnavbar-inner --> 
</div>
<!-- /subnavbar -->
