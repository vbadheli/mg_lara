<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Party extends Model
{
    protected $fillable = [
        'firm_name','firstname','lastname','email', 'phone','address','gst',
    ];
}
