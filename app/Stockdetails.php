<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stockdetails extends Model
{
    public function stockdata()
    {
        return $this->hasOne('App\Stock','id','stock_id');
    }
}
