<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Party;
use App\Stock;
use App\Stockdetails;
use App\Size;
use App\Cutting;
use App\Cuttingdetail;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $partydata = Party::get();          
        return view('stockpage.stock')->with('partydata', $partydata);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'party' => 'required',
            'dno' => 'required'
        ]);
        $stock=new Stock;
        $stock->party_id=$request->party;
        $stock->date=$request->date;
        $stock->dno=$request->dno;
        $stock->challan_no=$request->challanno;
        $stock->cloth_name=$request->cloth_name;
        if($stock->save()){
            $id=$stock->id;
            foreach ($request->size as $key => $v) 
            {
               $data=array('stock_id'=>$id,
                            'length'=>$v,
                            'color'=>$request->color[$key],
                            'photo'=>$request->photo[$key]);
               Stockdetails::insert($data);
            }
        }
        return back()->with('success','Submit Successfullly');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $sdata = Stock::with('party')->get();   
        return view('reports.stock')->with('sdata', $sdata);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function partyshow()
    {
        $partyshow = Party::get();
        return view('reports.party')->with('partyshow', $partyshow);
    }
    public function employeeshow()
    {
        $employeeshow = Employee::get();          
        return view('reports.employee')->with('employeeshow', $employeeshow);
    }
    public function sizeshow()
    {
        $sizeshow = Size::get();          
        return view('reports.size')->with('sizeshow', $sizeshow);
    }
    public function datashow()
    {
        $partydata = Party::get(); 
        $getsizes = Size::get(); 
        return view('pages.cutting')->with('partydata',$partydata)->with('getsizes', $getsizes);
    }
    public function view($id)
    {
        $stockdata = Stock::where('party_id',$id)->get();
        $data="<option value='' selected='true' disabled='true'>Select Design No.</option>";
        foreach ($stockdata as $key) {
            $data.= "<option value='$key->id'>$key->dno</option>";
        }      
        return $data;
    }   
    public function viewstockdetails($id)
    {
        $success = Stockdetails::where('stock_id',$id)->get();
        $data="";
        foreach ($success as $key) {
            $data.= "<tr>
                       
                       <td style='text-align:center'><input type='hidden' readonly='true' name='stockdetails_id[]' class='span3' value='$key->id' style='width: 45px;'><input type='text' readonly='true' name='sizename[]' class='span3 sizename' value='$key->color' style='width: 45px;'></td>
                       <td style='text-align:center'><input type='text' name='count1[]' class='span3 count' style='width: 45px;'></td>
                       <td style='text-align:center'><input type='text' name='count2[]' class='span3 count' style='width: 45px;'></td>
                       <td style='text-align:center'><input type='text' name='count3[]' class='form-control span3 count' style='width: 45px;'></td>
                       <td style='text-align:center'><input type='text' name='count4[]' class='form-control span3 count' style='width: 45px;'></td>
                       <td style='text-align:center'><input type='text' name='count5[]' class='form-control span3 count' style='width: 45px;'></td>
                       <td style='text-align:center'><input type='text'  readonly='' name='totalpic[]'  id='totalpic' class='form-control span3 totalpic' style='width: 45px;'></td>
                     </tr>";
        }
        return $data;
    }
    public function insert(Request $request)
    {        
         // dd($request->all());
        $this->validate($request, [
            'party_id' => 'required',
            'dno' => 'required'
        ]);
        $cutting=new Cutting;
        $cutting->party_id=$request->party_id;
        $cutting->dno=$request->dno;
       if($cutting->save()){
           $id=$cutting->id;           
            foreach ($request->stockdetails_id as $key => $v) 
            {
               $data=array('cutting_id'=>$id,
                        'stockdetail_id'=>$v,
                        'count'=>$request->count1[$key],
                        'size_id'=>$request->sizes1,
                        'size_id'=>$request->sizes2,
                        'size_id'=>$request->sizes3,
                        'size_id'=>$request->sizes4,
                        'size_id'=>$request->sizes5,
                        'count'=>$request->count2[$key],
                        'count'=>$request->count3[$key],
                        'count'=>$request->count4[$key],
                        'avg'=>$request->avg[$key],
                        'count'=>$request->count5[$key]);
                    Cuttingdetail::insert($data);
            }
         }
        return back()->with('success','Submit Successfullly');
    }    
   
}
