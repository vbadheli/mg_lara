<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Party;
use App\Stock;
use App\Stockdetails;
use App\Size;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('employee');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'phone' => 'required'
        ]);

        $data =Employee::create($request->all());
        return back()->with('success','Submit Successfullly');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $Employee=Employee::all();
        return view('pages.employee')->with('Employee', $Employee);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Employee::find($id);
        return view('pages.editemployee')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updatedata= Employee::find($id);
        $updatedata->firstname=$request->firstname;
        $updatedata->lastname=$request->lastname;
        $updatedata->email=$request->email;
        $updatedata->phone=$request->phone;
        $updatedata->address=$request->address;
        $updatedata->save();
        return redirect('employee')->with('success','Information has been Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Employee::find($id);
        $employee->delete();
        return redirect('employee')->with('danger','Information has been deleted');
    }
    public function EmployeeCount()
    {
        $empcount = Employee::count();        
        $partycount = Party::count();        
        $sumStock = Stockdetails::sum('length');
        return view('layout.home')->with('empcount',$empcount)->with('partycount',$partycount)->with('sumStock',$sumStock);
    }
    public function showdata()
    {
        $sizes=Size::all();
        return view('pages.size')->with('sizes', $sizes);
    }
    public function insert(Request $request)
    {
        $this->validate($request, [
            'size' => 'required'
        ]);

        $data =Size::create($request->all());
        return back()->with('success','Submit Successfullly');
    }
    public function sizeedit($id)
    {
        $data = Size::find($id);
        return view('pages.editsize')->with('data',$data);
    }
    public function sizeupdate(Request $request, $id)
    {
        $updatedata= Size::find($id);
        $updatedata->size=$request->size;        
        $updatedata->save();
        return redirect('size')->with('success','Information has been Updated');
    }
    public function delete($id)
    {
        $size = Size::find($id);
        $size->delete();
        return redirect('size')->with('danger','Information has been deleted');
    }
}
