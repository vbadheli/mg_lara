<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    public function party()
    {
        return $this->hasOne('App\Party','id','party_id');
    }
}
