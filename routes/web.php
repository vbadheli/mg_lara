<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('layout.home');
// });
Route::get('/', function () {
    return view('login');
});
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout');

Route::get('/home', ['as' => 'layout.home', 'uses' => 'EmployeeController@EmployeeCount']);
Route::get('/party', ['as' => 'pages.partys', 'uses' => 'PartyController@show']);
Route::get('/edit/{id}', ['as' => 'pages.editparty', 'uses' => 'PartyController@edit']);
Route::post('/update/{id}', ['as' => 'pages.editparty', 'uses' => 'PartyController@update']);
Route::delete('/destroy/{id}', ['as' => 'pages.partys', 'uses' => 'PartyController@destroy']);
Route::post('/store', ['as' => 'pages.partys', 'uses' => 'PartyController@store']);

Route::get('/employee', ['as' => 'pages.employee', 'uses' => 'EmployeeController@show']);
Route::get('/employee/edit/{id}', ['as' => 'pages.editemployee', 'uses' => 'EmployeeController@edit']);
Route::post('/employee/update/{id}', ['as' => 'pages.editemployee', 'uses' => 'EmployeeController@update']);
Route::delete('/employee/destroy/{id}', ['as' => 'pages.employee', 'uses' => 'EmployeeController@destroy']);
Route::post('/employee/store', ['as' => 'pages.employee', 'uses' => 'EmployeeController@store']);

Route::get('/stock', ['as' => 'stockpage.stock', 'uses' => 'StockController@index']);
Route::post('/stock/store', ['as' => 'stockpage.stock', 'uses' => 'StockController@store']);

Route::get('/report/show', ['as' => 'reports.stock', 'uses' => 'StockController@show']);
Route::get('/report/party/show', ['as' => 'reports.party', 'uses' => 'StockController@partyshow']);
Route::get('/report/employee/show', ['as' => 'reports.employee', 'uses' => 'StockController@employeeshow']);
Route::get('/report/size/show', ['as' => 'reports.size', 'uses' => 'StockController@sizeshow']);

Route::get('/size', ['as' => 'pages.size', 'uses' => 'EmployeeController@showdata']);
Route::post('/create', ['as' => 'pages.size', 'uses' => 'EmployeeController@insert']);
Route::get('/size/edit/{id}', ['as' => 'pages.size', 'uses' => 'EmployeeController@sizeedit']);
Route::post('/size/update/{id}', ['as' => 'pages.size', 'uses' => 'EmployeeController@sizeupdate']);
Route::delete('/size/destroy/{id}', ['as' => 'pages.size', 'uses' => 'EmployeeController@delete']);

Route::get('/cutting', ['as' => 'pages.cutting', 'uses' => 'StockController@datashow']);
Route::post('/cutting/store', ['as' => 'pages.cutting', 'uses' => 'StockController@insert']);
Route::get('/view/{id}', ['as' => 'pages.cutting', 'uses' => 'StockController@view']);
Route::get('/viewstockdetails/{id}', ['as' => 'pages.cutting', 'uses' => 'StockController@viewstockdetails']);
// Route::get('/getsizes', ['as' => 'pages.cutting', 'uses' => 'StockController@getsizes']);
