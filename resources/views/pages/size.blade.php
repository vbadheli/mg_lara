@extends('layout.app')
@section('content')
<div class="row">	
   <div class="span12">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>Add Sizes</h3>
         </div>
         <!-- /widget-header -->
         <div class="widget-content">
            <div class="tabbable">              
               <div class="tab-content">
                  <div class="tab-pane active" id="formcontrols">
                     <form id="edit-profile" class="form-horizontal" action="{{url('/create')}}" method="POST">
                     	@csrf
                        @if (Session::get('error'))
                           <div class="alert alert-danger">
                              {{ Session::get('error') }}
                           </div>
                           @endif
                           @if(session('success'))
                              <div class="alert alert-success">
                                {{ session('success') }}
                              </div>
                           @endif                           
                        <fieldset>
                           <div class="control-group">
                              <label class="control-label" for="firm_name">Size</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="size" placeholder="Enter Your sizes" name="size" required="true" autofocus="ture">
                              </div>
                              <!-- /controls -->				
                           </div>                           
                           <!-- /control-group -->
                           <div class="form-actions">
                              <button type="submit" class="btn btn-primary">Save</button> 
                              <button class="btn btn-default" type="reset">Reset</button>
                           </div>
                           <!-- /form-actions -->
                        </fieldset>
                     </form>
                  </div>                  
               </div>
            </div>    
            <div class="span11">
               <div class="widget-header">
                  <h3>Show Records</h3>
               </div>
               <!-- /widget-header -->
               <div class="widget-content">
                  <div class="card-body">
               <table class="table table-bordered">
                  <thead>
                     <tr>
                        <th>Sr.</th>                        
                        <th>Sizes</th>                        
                        <th>Created At</th>
                        <th>Edit</th>
                        <th>Delete</th>
                     </tr>
                  </thead>
                  <?php  $SrNo = 1; ?>
                  <tbody>
                     @foreach($sizes as $data)                   
                     <tr>
                        <td>{{$SrNo++}}</td>
                        <td>{{$data->size}}</td>                        
                        <td>{{$data->created_at->format('d/m/Y H:i:s')}}</td>                         
                        <td>
                           <a href="{{url('size/edit', $data['id'])}}" class="btn btn-warning btn-sm"><i class="icon-pencil"></i></a>
                        <td>
                           <form action="{{url('size/destroy', $data['id'])}}" method="post">
                              @csrf
                              <input name="_method" type="hidden" value="DELETE">
                              <button class="btn btn-danger btn-sm" type="submit"><i class="icon-remove"></i></button>
                            </form>
                        </td>                         
                     </tr>
                     @endforeach
                  </tbody>
               </table>             
            </div>
               </div>
            </div>
         </div>
         <!-- /widget-content -->
      </div>
      <!-- /widget -->
   </div>
   <!-- /span8 -->
</div>
<!-- /row -->
@endsection