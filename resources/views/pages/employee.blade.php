@extends('layout.app')
@section('content')
<div class="row">	
   <div class="span12">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>Add New Employees</h3>
         </div>
         <!-- /widget-header -->
         <div class="widget-content">
            <div class="tabbable">              
               <div class="tab-content">
                  <div class="tab-pane active" id="formcontrols">
                     <form id="edit-profile" class="form-horizontal" action="{{url('/employee/store')}}" method="POST">
                     	@csrf
                        @if (Session::get('error'))
                           <div class="alert alert-danger">
                              {{ Session::get('error') }}
                           </div>
                           @endif
                           @if(session('success'))
                              <div class="alert alert-success">
                                {{ session('success') }}
                              </div>
                           @endif
                           @if(session('danger'))
                              <div class="alert alert-danger">
                                {{ session('danger') }}
                              </div>
                           @endif
                        <fieldset>                           
                           <!-- /control-group -->
                           <div class="control-group">
                              <label class="control-label" for="firstname">First Name</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="firstname" name="firstname" placeholder="Enter Your First Name">
                              </div>
                              <!-- /controls -->				
                           </div>
                           <!-- /control-group -->
                           <div class="control-group">
                              <label class="control-label" for="lastname">Last Name</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="lastname" name="lastname" placeholder="Enter Your Last Name">
                              </div>
                              <!-- /controls -->				
                           </div>
                           <!-- /control-group -->
                           <div class="control-group">
                              <label class="control-label" for="email">Email Address</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="email" name="email" placeholder="Enter Valid Email Address">
                              </div>
                              <!-- /controls -->				
                           </div>
                           <!-- /control-group -->
                           <div class="control-group">
                              <label class="control-label" for="phone">Phone</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="phone" name="phone" placeholder="Enter phone Number" required="true">
                              </div>
                              <!-- /controls -->				
                           </div>
                           <!-- /control-group -->
                           <div class="control-group">
                              <label class="control-label" for="address">Address</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="address" name="address" placeholder="Enter Address">
                              </div>
                              <!-- /controls -->				
                           </div>
                           <!-- /control-group -->
                           <div class="form-actions">
                              <button type="submit" class="btn btn-primary">Save</button> 
                              <button class="btn btn-default" type="reset">Reset</button>
                           </div>
                           <!-- /form-actions -->
                        </fieldset>
                     </form>
                  </div>                  
               </div>
            </div>    
            <div class="span11">
               <div class="widget-header">
                  <h3>Show Records</h3>
               </div>
               <!-- /widget-header -->
               <div class="widget-content">
                  <div class="card-body">
               <table class="table table-bordered">
                  <thead>
                     <tr>
                        <th>Sr.</th>                        
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone No.</th>
                        <th>Address</th>
                        <th>Created At</th>
                        <th>Edit</th>
                        <th>Delete</th>
                     </tr>
                  </thead>
                  <?php  $SrNo = 1; ?>
                  <tbody>
                     @foreach($Employee as $data)                   
                     <tr>
                        <td>{{$SrNo++}}</td>
                        <td>{{$data->firstname}}  {{$data->lastname}}</td>
                        <td>{{$data->email}}</td>
                        <td>{{$data->phone}}</td>
                        <td>{{$data->address}}</td>
                        <td>{{$data->created_at->format('d/m/Y H:i:s')}}</td>                         
                        <td>
                           <a href="{{url('employee/edit', $data['id'])}}" class="btn btn-warning btn-sm"><i class="icon-pencil"></i></a>
                        <td>
                           <form action="{{url('employee/destroy', $data['id'])}}" method="post">
                              @csrf
                              <input name="_method" type="hidden" value="DELETE">
                              <button class="btn btn-danger btn-sm" type="submit"><i class="icon-remove"></i></button>
                            </form>
                        </td>                         
                     </tr>
                     @endforeach
                  </tbody>
               </table>             
            </div>

               </div>
            </div>
         </div>
         <!-- /widget-content -->
      </div>
      <!-- /widget -->
   </div>
   <!-- /span8 -->
</div>
<!-- /row -->
@endsection