@extends('layout.app')
@section('content')
@if(isset($data))
<div class="row">	
   <div class="span12">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>Edit Employee</h3>
         </div>
         <!-- /widget-header -->
         <div class="widget-content">
            <div class="tabbable">              
               <div class="tab-content">
                  <div class="tab-pane active" id="formcontrols">
                     <form id="edit-profile" class="form-horizontal" action="{{url('/employee/update',$data->id)}}" method="POST">
                     	@csrf
                        @if (Session::get('error'))
                           <div class="alert alert-danger">
                              {{ Session::get('error') }}
                           </div>
                           @endif
                           @if(session('success'))
                              <div class="alert alert-success">
                                {{ session('success') }}
                              </div>
                           @endif
                        <fieldset>                           
                           <!-- /control-group -->
                           <div class="control-group">
                              <label class="control-label" for="firstname">First Name</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="firstname" value="{{$data->firstname}}" name="firstname" placeholder="Enter Your First Name">
                              </div>
                              <!-- /controls -->				
                           </div>
                           <!-- /control-group -->
                           <div class="control-group">
                              <label class="control-label" for="lastname">Last Name</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="lastname" name="lastname" value="{{$data->lastname}}" placeholder="Enter Your Last Name">
                              </div>
                              <!-- /controls -->				
                           </div>
                           <!-- /control-group -->
                           <div class="control-group">
                              <label class="control-label" for="email">Email Address</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="email" name="email"  value="{{$data->email}}" placeholder="Enter Valid Email Address">
                              </div>
                              <!-- /controls -->				
                           </div>
                           <!-- /control-group -->
                           <div class="control-group">
                              <label class="control-label" for="phone">Phone</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="phone" name="phone" value="{{$data->phone}}" placeholder="Enter phone Number" required="true">
                              </div>
                              <!-- /controls -->				
                           </div>
                           <!-- /control-group -->
                           <div class="control-group">
                              <label class="control-label" for="address">Address</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="address" name="address" value="{{$data->address}}" placeholder="Enter Address">
                              </div>
                              <!-- /controls -->				
                           </div>
                           <!-- /control-group -->
                           <div class="form-actions">
                              <button type="submit" class="btn btn-info">Update</button> 
                              <a href="{{url('employee')}}" class="btn btn-default btn-sm">Cancel</a>
                           </div>
                           <!-- /form-actions -->
                        </fieldset>
                     </form>
                  </div>                  
               </div>
            </div>            
           
               </div>
            </div>
         </div>
         <!-- /widget-content -->
      </div>
      <!-- /widget -->
   </div>
   <!-- /span8 -->
</div>
<!-- /row -->
@endif
@endsection