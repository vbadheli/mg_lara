@extends('layout.app')
@section('content')
<div class="row">
   <div class="span12">
      <div class="widget">
         <div class="widget-header">
            <i class="icon-user"></i>
            <h3>Add Cutting Section</h3>
         </div>
         <!-- /widget-header -->
         <div class="widget-content">
            <div class="tabbable">
               <div class="tab-content">
                  <div class="tab-pane active" id="formcontrols">
                     <form id="edit-profile" class="form-horizontal" action="{{url('/cutting/store')}}" method="POST">
                        @csrf
                        @if (Session::get('error'))
                        <div class="alert alert-danger">
                           {{ Session::get('error') }}
                        </div>
                        @endif
                        @if(session('success'))
                        <div class="alert alert-success">
                           {{ session('success') }}
                        </div>
                        @endif                          
                        <fieldset>
                           <div class="control-group">
                              <label class="control-label" for="firm_name">Select Party/Firm</label>
                              <div class="controls">
                                 <select class="span4" id="party_id" name="party_id" autofocus="true" onChange="getState(this.value);" required="true">
                                    <option value="" selected="true" disabled="true">Select Party/Firm Name</option>
                                    @foreach($partydata as $data)
                                    <option value="{{$data->id}}">{{$data->firm_name}}</option>
                                    @endforeach
                                 </select>
                                 <a class="btn btn-success btn-mini" href="#" data-toggle="modal" data-target="#party_model">Add Party Name</a>
                              </div>
                              <!-- /controls -->            
                           </div>
                           <div class="control-group">
                              <label class="control-label" for="dno">Design No:</label>
                              <div class="controls">
                                 <select class="span4" id="dno" name="dno" autofocus="true" required="true" onChange="getColor(this.value);">
                                    <option value='' selected='true' disabled='true'>Select Design No.</option>
                                 </select>
                                 <a class="btn btn-success btn-mini" href="#" data-toggle="modal" data-target="#design_model">Add Design No.</a>
                              </div>
                              <!-- /controls -->            
                           </div>
                           <!-- <div class="control-group">
                              <label class="control-label" for="dno">Design No:</label>
                              <div class="controls">
                                 <input type="text" class="span4" id="dno" name="dno" placeholder="Enter Your Design Number" required="">                                
                              </div>
                               /controls -->            
                           <!-- </div> --> 
                           <!-- /control-group -->
                           <div class="form-actions">
                              <button type="submit" class="btn btn-primary">Save</button> 
                              <button class="btn btn-default" type="reset">Reset</button>
                           </div>
                           <!-- /form-actions -->
                        </fieldset>
                        <div class="span11" >
                           <table class='table table-bordered mtable' id="tblshow">
                              <thead>
                                 <tr>
                                    <th style='text-align:center'>Size Cutting</th>
                                    <th style='text-align:center'>
                                       <select class="span4" id="sizes" name="sizes1" autofocus="true" style='width: 70px;'>
                                          <option value="" selected="true" disabled="true">Sizes</option>
                                          @foreach($getsizes as $data)
                                          <option value="{{$data->id}}">{{$data->size}}</option>
                                          @endforeach
                                       </select>
                                    </th>
                                    <th style='text-align:center'>
                                       <select class="span4" id="sizes" name="sizes2" autofocus="true" style='width: 70px;'>
                                          <option value="" selected="true" disabled="true">Sizes</option>
                                          @foreach($getsizes as $data)
                                          <option value="{{$data->id}}">{{$data->size}}</option>
                                          @endforeach
                                       </select>
                                    </th>
                                    <th style='text-align:center'>
                                       <select class="span4" id="sizes" name="sizes3" autofocus="true" style='width: 70px;'>
                                          <option value="" selected="true" disabled="true">Sizes</option>
                                          @foreach($getsizes as $data)
                                          <option value="{{$data->id}}">{{$data->size}}</option>
                                          @endforeach
                                       </select>
                                    </th>
                                    <th style='text-align:center'>
                                       <select class="span4" id="sizes" name="sizes4" autofocus="true" style='width: 70px;'>
                                          <option value="" selected="true" disabled="true">Sizes</option>
                                          @foreach($getsizes as $data)
                                          <option value="{{$data->id}}">{{$data->size}}</option>
                                          @endforeach
                                       </select>
                                    </th>
                                    <th style='text-align:center'>
                                       <select class="span4" id="sizes" name="sizes5" autofocus="true" style='width: 70px;'>
                                          <option value="" selected="true" disabled="true">Sizes</option>
                                          @foreach($getsizes as $data)
                                          <option value="{{$data->id}}">{{$data->size}}</option>
                                          @endforeach
                                       </select>
                                    </th>
                                    <th><button  type="button" data-toggle="modal" data-target="#exampleModal" class="btn btn-success btn-xs" id="icol"><i class="icon-plus-sign"></i></button> Total (pcs.)</th>
                                 </tr>
                              </thead>
                              <tbody id="tablevalue">
                              </tbody>
                              <tfoot>
                                 <tr>
                                    <td style='text-align:center'>AVG</td>
                                    <td style='text-align:center'><input type='text' name='avg[]' id='Mavg'  class='form-control span3 avg' style='width: 45px;'></td>
                                    <td style='text-align:center'><input type='text' name='avg[]' id='Lavg'  class='form-control span3 avg' style='width: 45px;'></td>
                                    <td style='text-align:center'><input type='text' name='avg[]' id='XLavg' class='form-control span3 avg' style='width: 45px;'></td>
                                    <td style='text-align:center'><input type='text' name='avg[]' id='2XLavg' class='form-control span3 avg' style='width: 45px;'></td>
                                    <td style='text-align:center'><input type='text' name='avg[]' id='3XLaavgvg' class='form-control span3 avg' style='width: 45px;'></td>
                                    <td style='text-align:center'><input  readonly='' type='text' name='totalpicavg[]' id='totalpicavg' class='form-control span3 totalpicavg' style='width: 45px;'></td>
                                 </tr>
                                 <tr>
                                    <td style='text-align:center'>Total</td>
                                    <td style='text-align:center' id='totalM'></td>
                                    <td style='text-align:center' id='totalL'></td>
                                    <td style='text-align:center' id='totalXL'></td>
                                    <td style='text-align:center' id='totalTWOXL'></td>
                                    <td style='text-align:center' id='totalTHREEXL'></td>
                                    <td style='text-align:center' id='Sumoftotal' class='Sumoftotal'></td>
                                 </tr>
                              </tfoot>
                           </table>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- /widget-content -->
</div>
<!-- /widget -->
</div>
<!-- /span8 -->
</div>
<!-- <input type='text'  readonly=' name='totalpic[]'  id='totalpic' class='form-control span3 totalpic' style='width: 45px;'> -->
<!-- /row -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
   $("table").hide();
   
   $('#icol').click(function(){
     if($('#col').val()){
         $('.mtable tr').append($("<td>"));
         $('.mtable thead tr>td:last').html($('#col').val());
         var columnname=$('#col').val();
         $('.mtable tbody tr').each(function(){$(this).children('td:last').append($('<input type="text" name="'+columnname+'" class="form-control span3" style="width: 45px;">'))});
       }
       //else{alert('Enter Text');}
       resetEditor();
   });
   
    function getState(val) {
      $.ajax({
      type: "GET",
      url: "view/"+val,
      success: function(data){
      //alert(data);
        $("#dno").html(data);
      }
      });
    }
    function getColor(val) {
      $.ajax({
      type: "GET",
      url: "viewstockdetails/"+val,
      success: function(data){
        //alert(data);
        $("table").show();
        $("#tablevalue").html(data);
      }
      });
    }
       $(document).on('keydown keyup',function(){
          var totalsum=0;
          $('.M').each(function(){
             var inputVal=$(this).val();
             if($.isNumeric(inputVal)){
                totalsum+=parseFloat(inputVal);
             }
          });
          $("#totalM").text(totalsum);
       });
       $(document).on('keydown keyup',function(){
          var totalsum=0;
          $('.L').each(function(){
             var inputVal=$(this).val();
             if($.isNumeric(inputVal)){
                totalsum+=parseFloat(inputVal);
             }
          });
          $("#totalL").text(totalsum);
       });
       $(document).on('keydown keyup',function(){
          var totalsum=0;
          $('.XL').each(function(){
             var inputVal=$(this).val();
             if($.isNumeric(inputVal)){
                totalsum+=parseFloat(inputVal);
             }
          });
          $("#totalXL").text(totalsum);
       });
       $(document).on('keydown keyup',function(){
          var totalsum=0;
          $('.TWOXL').each(function(){
             var inputVal=$(this).val();
             if($.isNumeric(inputVal)){
                totalsum+=parseFloat(inputVal);
             }
          });
          $("#totalTWOXL").text(totalsum);
       });
       $(document).on('keydown keyup',function(){
          var totalsum=0;
          $('.THREEXL').each(function(){
             var inputVal=$(this).val();
             if($.isNumeric(inputVal)){
                totalsum+=parseFloat(inputVal);
             }
          });
          $("#totalTHREEXL").text(totalsum);
       });
       $(document).on('keydown keyup',function(){
          var grandtotal=0;
          $('.totalpic').each(function(){
             var inputVal=$(this).val();
             if($.isNumeric(inputVal)){
                grandtotal+=parseFloat(inputVal);
             }
          });
          $("#Sumoftotal").text(grandtotal);
       });
    
       // $('tbody').delegate('.M,.L,.XL,.TWOXL,.THREEXL','keyup',function(){
       //    var tr=$(this).parent().parent();
       //    var total=0;
       //    var M=tr.find('.M').val();
       //    var L=tr.find('.L').val();
       //    var XL=tr.find('.XL').val();
       //    var TWOXL=tr.find('.TWOXL').val();
       //    var THREEXL=tr.find('.THREEXL').val();
       //    total=M+L+XL+TWOXL+THREEXL;
       //    tr.find('.totalpic').val(total);
       // }); 
       function resetEditor()
       {
         $("#col").val("");
       }     
</script>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Add New Heading Column</h5>
            <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"> -->
            <!-- <span aria-hidden="true">&times;</span> -->
            <!-- </button> -->
         </div>
         <div class="modal-body">
            <input id="col" placeholder="Enter Heading"/>
            <button id="icol" class="btn btn-primary" data-dismiss="modal">Insert Column</button>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
         </div>
      </div>
   </div>
</div>
<!-- This is model is Party inseterd -->
<div class="modal fade" id="party_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <center>
               <h5 class="modal-title" id="exampleModalLongTitle">Add New Party Name</h5>
            </center>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <form id="edit-profile" class="form-horizontal" action="{{url('/store')}}" method="POST">
               @csrf
               @if (Session::get('error'))
               <div class="alert alert-danger">
                  {{ Session::get('error') }}
               </div>
               @endif
               @if(session('success'))
               <div class="alert alert-success">
                  {{ session('success') }}
               </div>
               @endif
               @if(session('danger'))
               <div class="alert alert-danger">
                  {{ session('danger') }}
               </div>
               @endif
               <fieldset>
                  <div class="control-group">
                     <label class="control-label" for="firm_name">Firm name</label>
                     <div class="controls">
                        <input type="text" class="span2" id="firm_name" placeholder="Enter Your firm_name" name="firm_name" required="true" autofocus="ture">
                     </div>
                     <!-- /controls -->            
                  </div>
                  <div class="control-group">
                     <label class="control-label" for="gst_no">GST NO.</label>
                     <div class="controls">
                        <input type="text" class="span2" id="gst" placeholder="Enter Your GST Number" name="gst">
                     </div>
                     <!-- /controls -->            
                  </div>
                  <!-- /control-group -->
                  <div class="control-group">
                     <label class="control-label" for="firstname">First Name</label>
                     <div class="controls">
                        <input type="text" class="span2" id="firstname" name="firstname" placeholder="Enter Your First Name">
                     </div>
                     <!-- /controls -->            
                  </div>
                  <!-- /control-group -->
                  <div class="control-group">
                     <label class="control-label" for="lastname">Last Name</label>
                     <div class="controls">
                        <input type="text" class="span2" id="lastname" name="lastname" placeholder="Enter Your Last Name">
                     </div>
                     <!-- /controls -->            
                  </div>
                  <!-- /control-group -->
                  <div class="control-group">
                     <label class="control-label" for="email">Email Address</label>
                     <div class="controls">
                        <input type="text" class="span2" id="email" name="email" placeholder="Enter Valid Email Address">
                     </div>
                     <!-- /controls -->            
                  </div>
                  <!-- /control-group -->
                  <div class="control-group">
                     <label class="control-label" for="phone">Phone</label>
                     <div class="controls">
                        <input type="text" class="span2" id="phone" name="phone" placeholder="Enter phone Number" required="true">
                     </div>
                     <!-- /controls -->            
                  </div>
                  <!-- /control-group -->
                  <div class="control-group">
                     <label class="control-label" for="address">Address</label>
                     <div class="controls">
                        <input type="text" class="span2" id="address" name="address" placeholder="Enter Address">
                     </div>
                     <!-- /controls -->            
                  </div>
                  <!-- /control-group -->
                  <!-- <div class="form-actions">
                     <button type="submit" class="btn btn-primary">Save</button> 
                     <button class="btn btn-default" type="reset">Reset</button>
                     </div> -->
                  <!-- /form-actions -->
                  <div class="modal-footer">
                     <button type="submit" class="btn btn-primary">Save</button>
                     <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  </div>
               </fieldset>
            </form>
         </div>
      </div>
   </div>
</div>
<!-- This is model is design inseterd -->
<div class="modal fade" id="design_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <center>
               <h5 class="modal-title" id="exampleModalLongTitle">Add New Desing Number</h5>
            </center>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <form id="edit-profile" class="form-horizontal" action="{{url('/stock/store')}}" method="POST">
               @csrf
               @if (Session::get('error'))
               <div class="alert alert-danger">
                  {{ Session::get('error') }}
               </div>
               @endif
               @if(session('success'))
               <div class="alert alert-success">
                  {{ session('success') }}
               </div>
               @endif
               @if(session('danger'))
               <div class="alert alert-danger">
                  {{ session('danger') }}
               </div>
               @endif
               <fieldset>
                  <div class="control-group">
                     <label class="control-label" for="firm_name">Select Party/Firm</label>
                     <div class="controls">
                        <select class="span3" id="party" name="party" autofocus="true" required="true" >
                           <option value="" selected="true" disabled="true">Select Party/Firm Name</option>
                           @foreach($partydata as $data)
                           <option value="{{$data->id}}">{{$data->firm_name}}</option>
                           @endforeach
                        </select>
                     </div>
                     <!-- /controls -->            
                  </div>
                  <!-- <a class="btn btn-success btn-mini" href="#" data-toggle="modal" data-target="#party_model">Add Party Name</a> -->
                  <div class="control-group">
                     <label class="control-label" for="date">Date</label>
                     <div class="controls">
                        <input type="date" class="span3" id="date" name="date" value="<?php echo date('Y-m-d');?>">
                     </div>
                     <!-- /controls -->            
                  </div>
                  <!-- /control-group -->
                  <div class="control-group">
                     <label class="control-label" for="dno">Design No:</label>
                     <div class="controls">
                        <input type="text" class="span3" id="dno" name="dno" placeholder="Enter Your Design Number" required="">
                     </div>
                     <!-- /controls -->            
                  </div>
                  <!-- /control-group -->
                  <div class="control-group">
                     <label class="control-label" for="challanno">Challan No.</label>
                     <div class="controls">
                        <input type="text" class="span3" id="challanno" name="challanno" placeholder="Enter Challan Number">
                     </div>
                     <!-- /controls -->            
                  </div>
                  <!-- /control-group -->
                  <div class="control-group">
                     <label class="control-label" for="cloth_name">Cloth Brand Name</label>
                     <div class="controls">
                        <input type="text" class="span3" id="cloth_name" name="cloth_name" placeholder="Enter Cloth Brand Name">
                     </div>
                     <!-- /controls -->            
                  </div>
                  <br>
                  <!-- /control-group -->
                  <!-- <div class="form-actions">
                     <button type="submit" class="btn btn-primary">Save</button> 
                     <button class="btn btn-default" type="reset">Reset</button>
                     </div> -->
                  <!-- /form-actions -->
               </fieldset>
               <div class="span11">
                  <table class="table table-bordered">
                     <thead>
                        <tr>
                           <th>Size/Mtr</th>
                           <th>Color</th>
                           <th>Photo</th>
                           <th style="text-align:center"><a href="#" class="btn btn-success addRow"><i class="icon-plus-sign"></i></a></th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td><input type="text"  placeholder="Enter Size Or Miter" name="size[]" class="span3 size"></td>
                           <td><input type="text" placeholder="Enter Color Name" name="color[]" class="span3 color"></td>
                           <td><input type="file" name="photo[]" class="form-control span3 photo"></td>
                           <td><a href="#" class="btn btn-danger remove"><i class="icon-remove"></i></a></td>
                        </tr>
                     </tbody>
                     <tfoot>
                        <tr>
                           <td id="total">Total</td>
                           <td style="border:none"></td>
                           <td style="border:none"></td>
                           <td style="border:none"></td>
                        </tr>
                     </tfoot>
                  </table>
               </div>
               <div class="modal-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">   
   $(".addRow").on("click",function(){
      addRow();
   });
   function addRow()
   {
      var addRow='<tr>'+
                  '<td><input type="text"  placeholder="Enter Size Or Miter" name="size[]" class="span3 size"></td>'+
                  '<td><input type="text" placeholder="Enter Color Name" name="color[]" class="span3 color"></td>'+
                  '<td><input type="file" name="photo[]" class="form-control span3 photo"></td>'+
                  '<td><a href="#" class="btn btn-danger remove"><i class="icon-remove"></i></a></td>'+
                  '</tr> ';
      $('tbody').append(addRow);
   }
   // function total()
   // {
   //    var total=0;
   //    $('.size').each(function(i,e){
   //       var size=$(this).val()-0;
   //       total+=size;
   //    })
   //    $('#total').text(total);
   // }
   $(document).on('keydown keyup',function(){
      var totalsum=0;
      $('.size').each(function(){
         var inputVal=$(this).val();
         if($.isNumeric(inputVal)){
            totalsum+=parseFloat(inputVal);
         }
      });
      $("#total").text(totalsum);
   });
   $('body').delegate('.remove','click',function(){
      var l=$('tbody tr').length;
      if(l==1)
      {
         alert("You can not remove last one");
      }else
         $(this).parent().parent().remove();
   });
</script>
@endsection