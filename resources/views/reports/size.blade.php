@extends('layout.app')
<style>
table, th, td {
    border: 1px solid black;
}
</style>
@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<h2><center>{{ __('Size Details') }}</center></h2>
				<div class="card-body">
					<table id="example" class="display" style="width:100%">
						<thead>
							<tr>
								<th style="text-align:center">Sr.</th>
								<th style="text-align:center">Size</th>								
								<th style="text-align:center">Created</th>								
							</tr>							
						</thead>
						<?php  $SrNo = 1; ?>
						<tbody>
							@foreach($sizeshow as $data)
							<tr>
								<td style="text-align:center">{{$SrNo++}}</td>																
								<td style="text-align:center">{{$data->size}}</td>								
								<td style="text-align:center">{{date('d-m-Y', strtotime($data->created_at))}}</td>								
							</tr>
							@endforeach
						</tbody>						
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
$(document).ready(function() {
  $('#example').DataTable({
    dom: 'Bfrtip',
    buttons: [
    {
      extend: 'copy',
      filename: 'sizes'
    }, {
      extend: 'csv',
      filename: 'sizes'
    },
    {
      extend: 'pdf',
      title: 'Size Details',
      filename: 'sizes'
    }, {
      extend: 'excel',
      title: 'Size Details',
      filename: 'sizes'
    },{
      extend: 'print',
      title: 'Size Details',
      filename: 'sizes'
    }]
  });
});
</script>
@endsection

