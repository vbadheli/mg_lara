@extends('layout.app')
<style>
table, th, td {
    border: 1px solid black;
}
</style>
@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<h2><center>{{ __('Stock Details') }} </center></h2>
				<div class="card-body">
					<table id="example" class="display" style="width:100%">
						<thead>
							<tr>
								<th>Sr.</th>
								<th>Party ID</th>
								<th>Design No</th>
								<th>Challan No</th>
								<th>Cloth Brand Name</th>
								<th>Date</th>								
							</tr>							
						</thead>
						<?php  $SrNo = 1; ?>
						<tbody>
							@foreach($sdata as $data)
							<tr>
								<td>{{$SrNo++}}</td>
								<td>{{$data->party->firm_name}}</td>								
								<td>{{$data->dno}}</td>
								<td>{{$data->challan_no}}</td>
								<td>{{$data->cloth_name}}</td>
								<td>{{date('d-m-Y', strtotime($data->date))}}</td>								
							</tr>
							@endforeach
						</tbody>						
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
$(document).ready(function() {
  $('#example').DataTable({
    dom: 'Bfrtip',
    buttons: [
    {
      extend: 'copy',
      filename: 'stock'
    }, {
      extend: 'csv',
      filename: 'stock'
    },
    {
      extend: 'pdf',
      title: 'Stock Details',
      filename: 'stock'
    }, {
      extend: 'excel',
      title: 'Stock Details',
      filename: 'stock'
    },{
      extend: 'print',
      title: 'Stock Details',
      filename: 'stock'
    }]
  });
});
</script>
@endsection


