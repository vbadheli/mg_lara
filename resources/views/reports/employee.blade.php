@extends('layout.app')
<style>
table, th, td {
    border: 1px solid black;
}
</style>
@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-12">
			<div class="card">
				<h2><center>{{ __('Employee Details') }} </center></h2>
				<div class="card-body">
					<table id="example" class="display" style="width:100%">
						<thead>
							<tr>
								<th>Sr.</th>
								<th>Name</th>
								<th>Email</th>
								<th>Mobile No</th>
								<th>Address</th>
								<th>Created</th>								
							</tr>							
						</thead>
						<?php  $SrNo = 1; ?>
						<tbody>
							@foreach($employeeshow as $data)
							<tr>
								<td>{{$SrNo++}}</td>
								<td>{{$data->firstname}}  {{$data->lastname}}</td>
								<td>{{$data->email}}</td>
								<td>{{$data->phone}}</td>								
								<td>{{$data->address}}</td>								
								<td>{{date('d-m-Y', strtotime($data->created_at))}}</td>								
							</tr>
							@endforeach
						</tbody>						
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
$(document).ready(function() {
  $('#example').DataTable({
    dom: 'Bfrtip',
    buttons: [
    {
      extend: 'copy',
      filename: 'employee'
    }, {
      extend: 'csv',
      filename: 'employee'
    },
    {
      extend: 'pdf',
      title: 'Employee Details',
      filename: 'employee'
    }, {
      extend: 'excel',
      title: 'Employee Details',
      filename: 'employee'
    },{
      extend: 'print',
      title: 'Employee Details',
      filename: 'employee'
    }]
  });
});
</script>
@endsection

